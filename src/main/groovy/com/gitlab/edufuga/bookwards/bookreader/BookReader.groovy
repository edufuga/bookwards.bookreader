package com.gitlab.edufuga.bookwards.bookreader

import com.gitlab.edufuga.bookwards.messageprinter.Colors
import com.gitlab.edufuga.bookwards.messageprinter.MessagePrinter
import com.gitlab.edufuga.bookwards.shuffler.SentenceShuffler
import com.gitlab.edufuga.wordlines.connectionreader.ConnectionReader
import com.gitlab.edufuga.wordlines.connectionwriter.ConnectionWriter
import com.gitlab.edufuga.wordlines.recordreader.RecordReader
import com.gitlab.edufuga.wordlines.recordreader.FileSystemRecordReader
import com.gitlab.edufuga.wordlines.recordwriter.RecordWriter
import com.gitlab.edufuga.wordlines.sentenceparser.LineParser
import com.gitlab.edufuga.wordlines.sentenceparser.SentenceParser

import com.gitlab.edufuga.wordlines.core.Status
import com.gitlab.edufuga.wordlines.core.WordStatusDate
import com.gitlab.edufuga.wordlines.unitreader.UnitReader
import com.gitlab.edufuga.wordlines.unitwriter.UnitWriter
import groovy.transform.CompileStatic
import org.fusesource.jansi.Ansi
import org.jline.terminal.Terminal
import org.jline.terminal.TerminalBuilder

import java.nio.file.Path
import java.nio.file.Paths

@CompileStatic
class BookReader {
    private FileSystemRecordReader recordReader
    private SentenceParser sentenceParser

    private Path bookPath
    private Terminal terminal

    private MessagePrinter messagePrinter

    private RecordWriter recordWriter

    private ConnectionWriter connectionWriter
    private String statusFolder

    private Map<String, RecordReader> toRecordReaders = new HashMap<>()
    private Map<String, RecordReader> coRecordReaders = new HashMap<>()

    private boolean mark = false
    private boolean reset = false

    private ConnectionReader connectionReader

    private String language

    private final UnitReader unitReader
    private final UnitWriter unitWriter

    private boolean color

    private boolean parseSentences

    private int lineNumber = 1
    private int goToLine = 1
    private int resetLineNumber = 1

    private boolean loadConnections

    private SentenceShuffler sentenceShuffler
    private boolean scrabble

    BookReader(Path bookPath, String statusFolder, Path sentencesFolder, String connectionsFolder, Path unitsFolder) {
        this.terminal = TerminalBuilder.builder().system(true).jansi(true).build()
        terminal.enterRawMode()

        this.bookPath = bookPath

        this.recordReader = new FileSystemRecordReader (statusFolder, sentencesFolder)
        this.sentenceParser = new SentenceParser (recordReader)

        this.messagePrinter = new MessagePrinter()

        this.recordWriter = new RecordWriter(statusFolder, recordReader)

        this.connectionWriter = new ConnectionWriter(connectionsFolder)
        this.statusFolder = statusFolder

        this.connectionReader = new ConnectionReader(connectionsFolder)

        this.language = Paths.get(statusFolder).getFileName ()

        this.unitReader = new UnitReader(unitsFolder)
        this.unitWriter = new UnitWriter(unitsFolder, recordReader)

        this.color = true

        this.parseSentences = true

        this.sentenceShuffler = new SentenceShuffler(sentenceParser, messagePrinter)
    }

    void read() {
        try (LineNumberReader br = new LineNumberReader(new FileReader(bookPath.toFile()))) {
            String line
            br.mark(200000)
            while ((line = br.readLine()) != null) {
                if (line == null || line.isEmpty()) {
                    messagePrinter.printMessage ("\n")
                }
                else {
                    boolean skipLine = lineNumber < goToLine
                    if (!skipLine) {
                        read(line)
                    }

                    if (mark) {
                        br.mark(200000)
                        mark = false
                        goToLine = lineNumber
                        resetLineNumber = lineNumber
                    }

                    if (reset) {
                        br.reset()
                        reset = false
                        Ansi.ansi().eraseScreen()
                        lineNumber = resetLineNumber
                        goToLine = resetLineNumber
                    }
                }
                lineNumber++
            }
        }
        catch (Exception ignored)
        {
            println "\nClosing..."
        }
        finally {
            terminal.close()
        }
    }

    void read(String line) {
        if (parseSentences) {
            List<String> sentences = LineParser.parseLineIntoSentences(line)
            for (String sentence : sentences) {
                final Map<Integer, Boolean> charactersToUnderline = new HashMap<>();

                List<WordStatusDate> recordsInSentence = sentenceParser.parseSentenceIntoRecords(sentence)

                // Underline words that have connections. This will *slow down the program considerably*.
                if (loadConnections) {
                    for (WordStatusDate record : recordsInSentence) {
                        String word = record.getWord()

                        // Read the connections only to see if there are some at all.
                        Map<String, List<String>> connections = connectionReader.read(language, word)

                        boolean hasConnections = !connections.isEmpty()
                        if (hasConnections) {
                            // Extend map of characters to underline with the characters of a single word.
                            // This map is used by the supplier that is passed to printMessage for the whole sentence.
                            // This way, the sentence is "extended word by word" with the information to underline.
                            charactersToUnderline.putAll(MessagePrinter.getCharactersToUnderline(sentence, word))
                        }
                    }
                }

                if (scrabble) {
                    try {
                        sentenceShuffler.shuffle(sentence)
                    }
                    catch (Exception ignored) {
                        messagePrinter.printMessage("Something went wrong while shuffling.")
                    }
                }
                else {
                    messagePrinter.printMessage(sentence, recordsInSentence,  () -> charactersToUnderline, false, color)
                }

                readCharacter()
            }
        }
        else {
            final Map<Integer, Boolean> charactersToUnderline = new HashMap<>();

            List<WordStatusDate> recordsInSentence = sentenceParser.parseSentenceIntoRecords(line)

            messagePrinter.printMessage(line, recordsInSentence, () -> charactersToUnderline, false, color)

            readCharacter()
        }
    }

    private void readCharacter() {
        String c = "" + (char) terminal.reader().read()
        if (c == "q") {
            throw new Exception("Interrupted.")
        }
        if (c == "e") {
            messagePrinter.printMessage("\n")
            String word = readWord()
            try {
                // Write the new record to a new file:
                WordStatusDate newRecord = askStatusAtCurrentDate(word)
                recordWriter.write(newRecord)

                // Ensure the cache is refreshed:
                recordReader.readRecord(word, false)
            }
            catch (Exception ignored) {
            }
        }
        if (c == "m") {
            mark = true
        }
        if (c == "n") {
            reset = true
        }
        if (c == "c") {
            println Ansi.ansi().eraseScreen().toString()
        }
        if (c == "f") {
            color = !color
        }
        if (c == "p") {
            parseSentences = !parseSentences
        }
        if (c == "t") {
            loadConnections = !loadConnections
        }
        if (c == "b") {
            scrabble = !scrabble
        }
        if (c == "r") {
            try {
                String from
                while (true) {
                    messagePrinter.printMessage("\n")
                    messagePrinter.printMessage("Word of current language?")
                    from = readWord()
                    messagePrinter.printMessage("\n")

                    if (from == "q") {
                        throw new Exception("Quitting")
                    }

                    // Transform word string to WordStatusDate object to see if it really is present in the folder of
                    // the current language (the from word belongs to the language of the book which is read here).
                    WordStatusDate fromRecord = recordReader.readRecord(from)
                    if (fromRecord.status == null) {
                        messagePrinter.printMessage("Record '$from' does not exist.\n")
                    } else {
                        break
                    }
                }

                String to
                String toStatusFolder = null
                while (true) {
                    toStatusFolder = null
                    messagePrinter.printMessage("Word to connect to?")
                    to = readWord()
                    messagePrinter.printMessage("\n")

                    if (to == "q") {
                        throw new Exception("Quitting")
                    }

                    while (toStatusFolder == null) {
                        messagePrinter.printMessage("Language of '$to'?")
                        String toLanguage = readWord().toUpperCase()
                        messagePrinter.printMessage("\n")

                        if (toLanguage == "q") {
                            throw new Exception("Quitting")
                        }

                        toStatusFolder = Paths.get(statusFolder).getParent().resolve(toLanguage).toAbsolutePath().toString()
                    }

                    RecordReader toReader = toRecordReaders.get(toStatusFolder)
                    if (toReader == null) {
                        toReader = new FileSystemRecordReader(toStatusFolder)
                        toRecordReaders.put(toStatusFolder, toReader)
                    }

                    // Transform word string to WordStatusDate object to see if it really present in the folder of the
                    // destination language (the to word belongs to any language which is present in the repository).
                    WordStatusDate toRecord = toReader.readRecord(to)
                    if (toRecord.status == null) {
                        messagePrinter.printMessage("Record '$to' does not exist.\n")
                    } else {
                        break
                    }
                }

                // Read the connection string. The connection is itself a record of a given language.
                String co
                String coStatusFolder = null
                while (true) {
                    coStatusFolder = null
                    messagePrinter.printMessage("Connection between '$from' and '$to'?")
                    co = readWord()
                    messagePrinter.printMessage("\n")

                    if (to == "q") {
                        throw new Exception("Quitting")
                    }

                    while (coStatusFolder == null) {
                        messagePrinter.printMessage("Language of '$co'?")
                        String coLanguage = readWord().toUpperCase()
                        messagePrinter.printMessage("\n")

                        if (coLanguage == "q") {
                            throw new Exception("Quitting")
                        }

                        coStatusFolder = Paths.get(statusFolder).getParent().resolve(coLanguage).toAbsolutePath().toString()
                    }

                    RecordReader coReader = coRecordReaders.get(coStatusFolder)
                    if (coReader == null) {
                        coReader = new FileSystemRecordReader(coStatusFolder)
                        coRecordReaders.put(coStatusFolder, coReader)
                    }

                    // Transform word string to WordStatusDate object to see if it really present in the folder of the
                    // connection language (the co word belongs to any language which is present in the repository).
                    WordStatusDate coRecord = coReader.readRecord(co)
                    if (coRecord.status == null) {
                        messagePrinter.printMessage("Record '$co' does not exist.\n")
                    } else {
                        break
                    }
                }

                connectionWriter.write(statusFolder, from, toStatusFolder, to, coStatusFolder, co)
            }
            catch (Exception ignored) {
                messagePrinter.printMessage("?")
            }
        }
        if (c == "k") {
            messagePrinter.printMessage("\nConnections for which word?")
            String word = readWord()
            messagePrinter.printMessage(" ")

            try {
                Map<String, List<String>> connections = connectionReader.read(language, word)
                connections.each { w, cons ->
                    cons.each { String con ->
                        messagePrinter.printMessage(con)
                    }
                }
            }
            catch (Exception ignored) {
                messagePrinter.printMessage("?")
            }
        }
        if (c == "x") {
            messagePrinter.printMessage("\nUnits for which word?")
            String word = readWord()
            messagePrinter.printMessage(" ")

            try {
                unitReader.read(word).each {
                    String w -> messagePrinter.printMessage(w)
                }
            }
            catch (Exception ignored) {
                messagePrinter.printMessage("?")
            }
        }
        if (c == "w") {
            try {
                String from
                while (true) {
                    messagePrinter.printMessage("\n")
                    messagePrinter.printMessage("From word of union?")
                    from = readWord()
                    messagePrinter.printMessage("\n")

                    if (from == "q") {
                        throw new Exception("Quitting")
                    }

                    // Transform word string to WordStatusDate object to see if it really present in the folder of the
                    // current language (the from word belongs to the language of the book which is read here).
                    WordStatusDate fromRecord = recordReader.readRecord(from)
                    if (fromRecord.status == null) {
                        messagePrinter.printMessage("Record '$from' does not exist.\n")
                    } else {
                        break
                    }
                }

                String to
                while (true) {
                    messagePrinter.printMessage("To word of union?")
                    to = readWord()
                    messagePrinter.printMessage("\n")

                    if (to == "q") {
                        throw new Exception("Quitting")
                    }

                    // Transform word string to WordStatusDate object to see if it really present as well.
                    WordStatusDate toRecord = recordReader.readRecord(to)
                    if (toRecord.status == null) {
                        messagePrinter.printMessage("Record '$to' does not exist.\n")
                    } else {
                        break
                    }
                }

                unitWriter.write(from, to)
            }
            catch (Exception ignored) {
                messagePrinter.printMessage("?")
            }
        }
        if (c == "l") {
            messagePrinter.printMessage("\nGo to line number: ")
            int number = readLineNumber()
            goToLine = number
        }
        if (c == "s") {
            messagePrinter.printMessage("\nLine number: $lineNumber.")
        }
    }

    private String readWord() {
        String word = ""
        Character c
        while ((c = Character.valueOf((char) terminal.reader().read())).isLetter())
        {
            word += c
            messagePrinter.printMessage("$c", Colors.noopt, false)
        }

        return word
    }

    private WordStatusDate askStatusAtCurrentDate(String word) {
        messagePrinter.printMessage ("? ", Colors.noopt, false)
        Status status = Status.UNKNOWN
        boolean  statusFound = false
        while (!statusFound) {
            Character c = Character.valueOf((char) terminal.reader().read())
            if (c == ("q" as char)) {
                throw new Exception ("Interrupted.")
            }

            status = Status.getFor(c)
            Objects.requireNonNull(status)
            statusFound = true
            messagePrinter.printMessage (status.toString()[0], Colors.noopt, false)
        }

        Date date = new Date()
        return new WordStatusDate(word, status, date)
    }

    private int readLineNumber() {
        try {
            String word = ""
            Character c
            while ((c = Character.valueOf((char) terminal.reader().read())).isDigit()) {
                word += c
                messagePrinter.printMessage("$c", Colors.noopt, false)
            }
            return word.toInteger()
        }
        catch (Exception ignored)
        {
            return lineNumber
        }
    }

    static void main(String[] args) {
        Terminal terminal = TerminalBuilder.terminal()
        PrintWriter writer = terminal.writer()

        writer.write("Please enter some text")
        writer.flush()
        String line = terminal.reader().readLine()
        println "You entered: "
        println line

        writer.close()
        terminal.close()
    }
}
